class State:
    "Class for Project states"
    self.Definite = 1
    self.Indefinite = 2

class Priority:
    "Class for defining priority"
    self.ExtremelyHigh = 1  # Urgent and Important
    self.VeryHigh = 2       # Urgent and non important
    self.High = 3           # Non urgent and Important
    self.Low = 4            # Non urgent and Non important
