from prime_enums import *

class Project:
    "Project class containing the project structure"

    def __init__(self):
        
        # Descriptive information
        self.title = "Untitled"
        self.subject = "Project"
        self.desc = "Lorem ipsum dolar sit"
        self.state = States.Indefinite

        # checkpoint regarding information
        self.total_checkpoints = 0
        self.acc_checkpoints = 0

        # information regarding the directory
        self.no_of_dir = 0
        self.no_of_files = 0
        self.path_dir = "c:\\Projects"
        self.priority = Priority.Low

        # time related information
        self.created = "NA" # python date here
        self.finished = "NA"
        self.last_modified = "NA"
        self.deadline = "NA"
        self.total_time_invested = "NA"

        # stored checkpoints
        self.checkpoints = []   # checkpoints object list
