from prime_enums import Priority

class Checkpoint:
    "Class to define a single checkpoint for a project"
    self.title = "Untitled"
    self.subject = "Untitled"
    self.desc = "NA"
    
    self.curr_date = "" #python date here
    self.deadline = ""  #

    self.no_of_files_modified = 0
    self.no_of_dir_modified = 0

    self.priority = Priority.Low
